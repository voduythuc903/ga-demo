package myapp;

import java.io.IOException;
import javax.servlet.http.*;

public class DemoServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        resp.setContentType("text/plain");
        resp.getWriter().println("{ \"name\": \"Hello World To Do List From Server \" }");
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
    		throws IOException{

    	resp.getWriter().println("{\"msg\": \"ok\"}");
    }
}
