var DATA = [];

function saveData(){
  localStorage['data'] = JSON.stringify(DATA);
};
 function _delete(item, it)
 {
	 $(item).parent().parent().remove();
	 DATA.splice(it,1);
 };
$(document).ready(function() {

  $.getJSON('/demo', function(data) {
      console.log('data:', data);
      $('#result').html(data.name);
    });

  //get todo data
  function getList(){
    //get data from local storage
    $('#lst').empty();
    var data = localStorage['data'];
    if (data) data = $.parseJSON(data);
    if (data){
      for (var i=0; i< data.length; i++){
        var item = data[i];
        $('#lst').append('<tr><td>'+item.name+'</td> <td><a href="#" onclick = "_delete(this,'+i+')">Delete</a> </td></tr>');
        DATA.push(item);
      }


    }
  }

  getList();
  
  //add a todo
  $('#btnAdd').click(function(){
    var name = $('#txt').val();
	if (name!= null && name != "")
	{
    $('#lst').append('<tr><td>'+name+'</td> <td><a href="#" onclick = "_delete(this,'+DATA.length+')">Delete</a> </td></tr>');
    DATA.push({name: name});
	}
	$('#txt').val("");
  });

  
});