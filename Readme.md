Hướng dẫn deploy Google App Engine

Yêu cầu: Windows, Java 7, Maven 3, Tài khoản Google+.

*Các bước thực hiện:

1. Tải gói google app engine sdk: https://storage.googleapis.com/appengine-sdks/featured/appengine-java-sdk-1.9.25.zip
2. Giải nén file appengine-java-sdk-1.9.25.zip và D:/appengine-java-sdk-1.9.25
3. Thêm đường dẫn D:/appengine-java-sdk-1.9.25/bin vào biến môi trường $PATH
4. Vào thư mục của project ga-demo, chạy lệnh: mvn clean install để tải các gói maven cho project. Sau khi install thành công, ứng dụng có thể thử ở local bằng lệnh `mvn appengine:devserver`
5. Vào đường dẫn appspot.com, đăng nhập bằng tài khoản Google+ và tạo 1 host trên Google App Engine. Có thể bỏ qua bước này và sử dụng host có sẵn với id: `thucvd001`
6. Vào thư mục của project ga-demo và chạy lệnh `appcfg.cmd -A thucvd001 update target\appengine-try-java-1.0`
7. Ứng dụng giờ đã được deploy tại đường dẫn: `http://thucvd001.appspot.com`