$(document).ready(function() {

  //get todo data
  function getList(){
    $.getJSON('/demo', function(data) {
      console.log('data:', data);
      $('#lst').empty()
      for (var i=0; i<data.length; i++){
        var item = data[i];
        $('#lst').append("<li>" + item.name + "</li>");
      }
      
    });
  }

  getList();
  
  //add a todo
  $('#btnAdd').click(function(){
    var postData = {
      name: $('#txt').val()
    };
    $.post('/demo', postData)
    .done(function(data){
      console.log('afterPost:', data);
      console.log('post success, refresh data');
      getList();
    })
  });

  
});